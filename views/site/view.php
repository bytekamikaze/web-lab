<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use app\models\View;

/* @var $this yii\web\View */
/* @var $dataProvider app\models\View */
/* @var $isDone app\models\View */
/* @var $model app\models\View */

?>

<div class="direction-view">

    <div class='btn btn-primary loadbtn'>Загрузить данные</div>
    <div class='btn btn-warning linkbtn'>Отчет</div>

    <?php $form = ActiveForm::begin([

    ]);
    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'layout' => "{pager}\n{items}",
        'columns' => [
            [
                'attribute' => 'NAME',
                'value' => function ($data) {
                    $testnorm = View::getTestNorm(Yii::$app->request->queryParams['id'], $data->LRTID, $data->ID);
                    if ($testnorm == '') {
                        return $data->NAME;
                    } else {
                        return $data->NAME . ' (' . $testnorm . ')';
                    }
                },
            ],
            [
                'header' => 'Значение',
                'content' => function ($data) use ($form) {
                    $value = View::getValueLrResult(Yii::$app->request->queryParams['id'], $data->LRTID, $data->ID);
                    $items = View::getDropLrListValues($data->ID);

                    if (empty($items)) {
                        return $form
                            ->field($data, 'NAME', ['inputOptions' => ['class' => 'form-control', 'name' => $data->ID]])
                            ->textInput(['value' => $value])
                            ->label(false);
                    } else {
                        return $form
                            ->field($data, 'NAME', ['inputOptions' => ['class' => 'form-control', 'name' => $data->ID]])
                            ->textInput(['value' => ''])
                            ->label(false)
                            ->dropDownList($items, ['prompt' => $value]);
                    }
                },
            ],

        ]
    ])

    //echo $form->field($model1, 'operation')->hiddenInput(['value' => 'hidden value']);

    ?>

    <?php ActiveForm::end(); ?>

    <?= Html::checkbox('isdone', $isDone, [
        'value' => $isDone,
        'label' => 'Отметка о выполнении',
        'class' => 'done'
    ]); ?>

</div>