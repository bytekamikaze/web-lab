<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\Direction */
/* @var $dropListLab app\models\Direction */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = 'Направления на анализ';
?>
<div class="direction-index">
    <div class="topBlock">
        <h1><?= Html::encode($this->title) ?></h1>
        <?php
        echo $this->render('_search', [
                'searchModel' => $searchModel,
                'dropListLab' => $dropListLab
            ]);
        function ToTime($value)
        {
            return date('d.m.Y', ($value - 25569) * 86400);
        }
        ?>
    </div>

    <div class="centerBlock">

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'layout' => "{pager}\n{items}\n{summary}",
            'summary' => "{begin} - {end} из {totalCount} элементов",
            'tableOptions' => [
                'pagination' => 10,
                'class' => 'table table-striped table-bordered tableDirection'
            ],
            /*        'rowOptions' => function ($data) {
                        return ['lrname' => $data->lrlist->NAME];
                    },*/

            'columns' => [
                /*            [
                                'class' => 'yii\grid\ActionColumn',
                            ],*/
                'code',
                [
                    'attribute' => 'medcard.GENDERTYPE',
                    'value' => function ($data) {
                        return $data->medcard->GENDERTYPE == 0 ? 'М' : 'Ж';
                    },
                    'contentOptions' => ['class' => 'gendertype'],

                ],
                [
                    'attribute' => 'medcard.NUMBER',
                    'contentOptions' => ['class' => 'mcid'],
                ],
                [
                    'attribute' => 'BARCODE',
                    'contentOptions' => ['class' => 'barcode'],
                ],

                'medcard.NAME',
                'medcard.NAME1',
                'medcard.NAME2',
                [
                    'attribute' => 'medcard.BIRTHDATE',
                    'value' => function ($data) {
                        return ToTime($data->medcard->BIRTHDATE);
                    }
                ],
                [
                    'attribute' => 'medcard.FULLNAME',
                    'contentOptions' => ['style' => 'display: none;', 'class' => 'fullname'],
                    'headerOptions' => ['style' => 'display: none;'],
                ],
                [
                    'header' => 'Возраст',
                    'value' => function ($data) {
                        $birthday = date('Y', ($data->medcard->BIRTHDATE - 25567) * 86400);
                        $age = date('Y', time()) - $birthday;
                        return $age;
                    },
                    'contentOptions' => ['class' => 'fullyears'],

                ],

                'TESTTUBENUMBER',
                'laboratory.NAME',
                [
                    'attribute' => 'lrlist.NAME',
                    'contentOptions' => function ($data) {
                        return ['class' => 'lrlistname', 'lrlid' => $data->LRLID];
                    }
                ],
                [
                    'label' => 'Отм. о вып.',
                    'attribute' => 'ISDONE',
                    'value' => function ($data) {
                        return $data->ISDONE == 0 ? 'нет' : 'да';
                    },
                    'contentOptions' => function ($data) {
                        return ['isdone' => $data->ISDONE];
                    }
                ],
                [
                    'attribute' => 'DODATETIME',
                    'value' => function ($data) {
                        return ToTime($data->DODATETIME);
                    },
                ],
                [
                    'attribute' => 'HTMLDATA',
                    'contentOptions' => ['style' => 'display: none;', 'class' => 'htmldata'],
                    'headerOptions' => ['style' => 'display: none;'],
                ],
            ],
        ]); ?>
    </div>

    <div class="mainTable"></div>
    <div class="resultTable"></div>
    <div class='selectFile'></div>

</div>
