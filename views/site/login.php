<?php
use yii\widgets\ActiveForm;

/* @var $login_model app\models\Login */

?>

<?php
$form = ActiveForm::begin([
    'options' => ['class' => 'formLogin'],
]);
?>

<div class='divLogin'>

    <?= $form->field($login_model, 'username')->textInput() ?>
    <?= $form->field($login_model, 'password')->passwordInput() ?>

    <button type="submit" class="btn btn-success">Войти</button>

</div>

<?php
yii\widgets\ActiveForm::end();
?>
