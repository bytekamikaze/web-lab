<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel app\models\Direction */
/* @var $dropListLab array */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="direction-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]);

    ?>
    <div class="fields-block">
        <div class="b1">
            <?= $form->field($searchModel, 'medcard.NUMBER') ?>
            <?= $form
                ->field($searchModel, 'LABORATORY')
                ->dropDownList($dropListLab, ['prompt' => 'ВСЕ']
                ); ?>
            <?= $form->field($searchModel, 'code') ?>
        </div>
        <div class="b2">
            <?= $form->field($searchModel, 'medcard.FULLNAME') ?>
            <?= $form->field($searchModel, 'BARCODE') ?>
        </div>
        <div class="b3">
            <?= $form->field($searchModel, 'TESTTUBENUMBER') ?>
            <?= $form->field($searchModel, 'ISDONE')->dropDownList([
                '1' => 'да',
                '0' => 'нет',
            ],
                ['prompt' => 'ВСЕ',]
            ); ?>
        </div>
        <div class="button-group">
            <?= Html::submitButton('Найти', ['class' => 'btn btn-primary']) ?>
            <?= Html::resetButton('Сброс', ['class' => 'btn btn-default']) ?>
        </div>
    </div>


    <?php ActiveForm::end(); ?>

</div>
