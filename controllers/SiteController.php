<?php

namespace app\controllers;

use yii;
use yii\web\Controller;
use app\models\Login;
use app\models\Direction;
use app\models\View;
use app\models\Insert;
use app\models\Alias;
use yii\filters\VerbFilter;

class SiteController extends Controller
{
    public function actionIndex()
    {
        //return $this->render('index');
        if (!Yii::$app->user->isGuest) {
            return $this->actionDirection();
        }

        $login_model = new Login();

        if (Yii::$app->request->post('Login')) {
            $login_model->attributes = Yii::$app->request->post('Login');

            if ($login_model->validate()) {
                Yii::$app->user->login($login_model->getUser());

                return $this->actionDirection();
            }
        }

        return $this->render('login', ['login_model' => $login_model]);

    }

    public function actionTest()
    {   //IA@MGEHmJBK
        $hash = md5('101', TRUE);
        print_r($hash);
        echo htmlentities($hash, ENT_QUOTES);
        echo htmlentities(md5($hash . '123', TRUE), ENT_QUOTES);

    }

    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            Yii::$app->user->logout();
            return $this->goHome();
        } else
            return $this->goHome();
    }

    public function actionLogout()
    {
        if (!Yii::$app->user->isGuest) {
            Yii::$app->user->logout();
            return $this->goHome();
        } else
            return $this->goHome();
    }


    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function actionDirection()
    {
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $searchModel = new Direction();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dropListLab = $searchModel->getDropListLab();

        return $this->render('direction', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'dropListLab' => $dropListLab
        ]);
    }

    public function actionView($id) // LR id
    {
        $isdone = View::checkIsDone($id);
        $this->layout = false;

        $viewModel = new View();
        $dataProvider = $viewModel->search(Yii::$app->request->queryParams);

        return $this->render("view", [
            'dataProvider' => $dataProvider,
            'viewModel' => $viewModel,
            'isDone' => $isdone,
        ]);
    }

    public function actionInsert()
    {
        $viewModel = new Insert();
        $viewModel->search(Yii::$app->request->queryParams);

        /* return $this->render('insert', [
             'data' => $data,
         ]);*/
        //return $this->redirect(['index']);
    }

    public function actionAlias()
    {
        $aliasModel = new Alias();
        $data = $aliasModel->search(Yii::$app->request->queryParams);
        echo $data;
    }

    public function actionFile()
    {
        $scriptPath = getcwd();
        $baseDir = dirname($scriptPath);
        $data = Yii::$app->request->queryParams;
        $dir = $baseDir . '\\_results\\dtlite';

        function str_search($path, $extension, $str)
        {
            $file_arr = array();
            foreach (glob(rtrim($path, '/') . '\*' . $extension) as $filename) {
                if (strstr(file_get_contents($filename), $str) != false)
                    $file_arr[] = $filename;
            }
            return $file_arr;
        }

        $srch = str_search($dir, $data['xml'], $data['barcode']);
        //var_dump($srch);

        foreach ($srch as $key => $value) {
            $srch[$key] = iconv("CP1251", "UTF-8", basename($value));
        }

        //var_dump(json_encode(basename($srch[0])));
        //var_dump($srch);

        echo json_encode($srch);

        //echo basename($srch[0]);
    }

}

?>
