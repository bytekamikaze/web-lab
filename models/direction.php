<?php

namespace app\models;

use yii;
use yii\base\Model;
use yii\db\ActiveRecord;
use yii\data\ActiveDataProvider;

class Direction extends ActiveRecord
{
    public static function tableName()
    {
        return 'LABORATORY_RESEARCHES';
    }
 
    public function attributes()
    {
        // add related fields to searchable attributes
        return array_merge(parent::attributes(),
            ['medcard.NAME'],
            ['medcard.NAME1'],
            ['medcard.NAME2'],
            ['medcard.FULLNAME'],
            ['medcard.NUMBER'],
            ['medcard.GENDERTYPE'],
            ['medcard.NUMBER'],
            ['laboratory.BIRTHDATE'],
            ['laboratory.NAME'],
            ['lrlist.NAME']
        );
    }

    public function attributeLabels()
    {
        return [
            'code' => 'Код посещения',
            'medcard.GENDERTYPE' => 'Пол',
            'medcard.NUMBER' => 'Номер карты',
            'BARCODE' => 'Штрих-код',
            'medcard.NAME' => 'Фамилия',
            'medcard.NAME1' => 'Имя',
            'medcard.NAME2' => 'Отчество',
            'medcard.FULLNAME' => 'Ф.И.О',
            'medcard.BIRTHDATE' => 'Дата рождения',
            'TESTTUBENUMBER' => 'Номер пробирки',
            'LABORATORY' => 'Лаборатория',
            'laboratory.NAME' => 'Лаборатория',
            'lrlist.NAME' => 'Наименование исследования',
            'ISDONE' => 'Отм. о выполн.',
            'DODATETIME' => 'Дата исследования',
            'HTMLDATA' => 'html результат',
        ];
    }

    public function rules()
    {
        return [
            [[
                'medcard.NUMBER',
                'medcard.FULLNAME',
                //'laboratory.NAME',
                'ISDONE',
                'BARCODE',
                'TESTTUBENUMBER',
                'code',
            ],
                'string'],
            [['LABORATORY'], 'number']
            //[['DODATETIME'], 'date', 'format' => 'dd.mm.yyyy'],

        ];

    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {

        function TimeStamp($value)
        {
            return strtotime($value) / 86400 + 25569 - 2; //было 25567
        }

        $this->load($params);

        $query = Direction::find()->from('LABORATORY_RESEARCHES labres')
            ->joinWith('medcard med')
            ->joinWith('laboratory lab')
            ->joinWith('lrlist lrl');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'attributes' => [

                    'code' => [
                        'asc' => ['labres.code' => SORT_ASC],
                        'desc' => ['labres.code' => SORT_DESC],
                    ],
                    'TESTTUBENUMBER' => [
                        'asc' => ['labres.TESTTUBENUMBER' => SORT_ASC],
                        'desc' => ['labres.TESTTUBENUMBER' => SORT_DESC],
                    ],
                    'BARCODE' => [
                        'asc' => ['labres.BARCODE' => SORT_ASC],
                        'desc' => ['labres.BARCODE' => SORT_DESC],
                    ],
                    'DODATETIME' => [
                        'asc' => ['labres.DODATETIME' => SORT_ASC],
                        'desc' => ['labres.DODATETIME' => SORT_DESC],
                    ],
                    'ISDONE' => [
                        'asc' => ['labres.ISDONE' => SORT_ASC],
                        'desc' => ['labres.ISDONE' => SORT_DESC],
                    ],
                    'medcard.NAME' => [
                        'asc' => ['med.NAME' => SORT_ASC],
                        'desc' => ['med.NAME' => SORT_DESC],
                    ],
                    'medcard.NAME1' => [
                        'asc' => ['med.NAME1' => SORT_ASC],
                        'desc' => ['med.NAME1' => SORT_DESC],
                    ],
                    'medcard.NAME2' => [
                        'asc' => ['med.NAME2' => SORT_ASC],
                        'desc' => ['med.NAME2' => SORT_DESC],
                    ],
                    'medcard.NUMBER' => [
                        'asc' => ['med.NUMBER' => SORT_ASC],
                        'desc' => ['med.NUMBER' => SORT_DESC],
                    ],
                    'medcard.GENDERTYPE' => [
                        'asc' => ['med.GENDERTYPE' => SORT_ASC],
                        'desc' => ['med.GENDERTYPE' => SORT_DESC],
                    ],
                    'medcard.BIRTHDATE' => [
                        'asc' => ['med.BIRTHDATE' => SORT_ASC],
                        'desc' => ['med.BIRTHDATE' => SORT_DESC],
                    ],
                    'laboratory.NAME' => [
                        'asc' => ['lab.NAME' => SORT_ASC],
                        'desc' => ['lab.NAME' => SORT_DESC],

                    ],
                    'lrlist.NAME' => [
                        'asc' => ['lrl.NAME' => SORT_ASC],
                        'desc' => ['lrl.NAME' => SORT_DESC],
                    ],

                ],

                'defaultOrder' => [
                    'DODATETIME' => SORT_DESC,
                    'ISDONE' => SORT_ASC,
                ]
            ],
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            //$query->where('0=1')
            return $dataProvider;
        }

        $query->andFilterWhere([

            'labres.LABORATORY' => $this->LABORATORY,
            'labres.ISDONE' => $this->ISDONE,

        ]);

        $query->andFilterWhere(['like', 'med.NUMBER', $this->getAttribute('medcard.NUMBER')]);
        $query->andFilterWhere(['like', 'med.FULLNAME', $this->getAttribute('medcard.FULLNAME')]);
        $query->andFilterWhere(['like', 'labres.TESTTUBENUMBER', $this->getAttribute('TESTTUBENUMBER')]);
        $query->andFilterWhere(['like', 'labres.BARCODE', $this->getAttribute('BARCODE')]);
        $query->andFilterWhere(['like', 'labres.code', $this->getAttribute('code')]);

        return $dataProvider;

    }

    public function getMedcard()
    {
        return $this->hasOne(Medcards::className(), ['ID' => 'MCID']);

    }

    public function getLaboratory()
    {
        return $this->hasOne(Laboratories::className(), ['ID' => 'LABORATORY']);

    }

    public function getLrlist()
    {
        return $this->hasOne(Lr_list::className(), ['ID' => 'LRLID']);

    }
    
    public static function getDropListLab() {

        $query = Direction::find()->from('LABORATORIES')
            ->select('NAME')
            ->where(['ENABLED' => 1])
            ->indexBy('ID')->column();

        return $query;
    }
}

class Medcards extends ActiveRecord
{

    public static function tableName()
    {
        return 'MEDCARDS';
    }
}

class Laboratories extends ActiveRecord
{

    public static function tableName()
    {
        return 'LABORATORIES';
    }

}

class Lr_list extends ActiveRecord
{

    public static function tableName()
    {
        return 'LR_LIST';
    }

}

class Lr_result extends ActiveRecord
{

    public static function tableName()
    {
        return 'LR_RESULT';
    }

}


