<?php
namespace app\models;

use yii\base\Model;

class Login extends Model 
{
    public $username;
    public $password;

    public function rules()
    {
        return [
            [['username','password'],'required', 'message' => 'Поле не заполнено'],
            ['password','validatePassword'],
        ];
    }
    public function attributeLabels()
    {
        return [
            'username' => 'Пользователь',
            'password' => 'Пароль',
        ];
    }
    public function validatePassword($attribute,$params)
    {
        if (!$this->hasErrors()) //если не ошибок то проводим валидацию
        {
            $user = $this->getUser();//получаем пользователя
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute,'Не верный пароль или имя пользователя');
                }
        }
    }

    public function getUser()
    {
        return User::findOne(['LOGIN'=>$this->username]);
    }
}
 ?>
