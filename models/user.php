<?php namespace app\models;

use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

class User extends ActiveRecord Implements IdentityInterface
{
    public static function tableName()
    {
        return 'USERS';
    }

    public function validatePassword($password)
    {
        return $this->PASSWORD === $password;
    }

    //--------------------------------------------------------------
    public static function findIdentity($id)
    {
        return self::findOne($id);
    }

    public function getId()
    {
        return $this->ID;
    }

    public function getAuthKey()
    {
        # code...
    }

    public function validateAuthKey($authKey)
    {
        # code...
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        # code...
    }

}

?>
