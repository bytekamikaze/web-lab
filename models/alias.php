<?php
namespace app\models;

use yii;
use yii\base\Model;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

class Alias extends ActiveRecord
{
    public static function tableName()
    {
        return 'LR_LIST_TEST_FIELD_APP_ALIAS';
    }


    public function attributeLabels()
    {
        return [

        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {

        $this->load($params);
        //var_dump($params);

        $LRTID = Alias::find()
            ->select('LRTESTID')
            ->from('LABORATORY_RESEARCHES_TEST')
            ->where(['LRID' => $params['LRID'], 'ENABLED' => 1])
            ->column();

		//var_dump($LRTID);
        foreach ($params[0] as $key => $value) {
         
         //$number = intval($value[0]);
        
            $id = Alias::find()
                ->select('LRTFID')
                //->where(['LRTFALIASNAME' => $value[0], 'LRTID' => $LRTID])
                ->where(['LRTFNUMBER' => $value[0]])
                ->Andwhere(['in', 'LRTID', $LRTID])
                ->scalar();

            if ($id == false) {
                $id = Alias::find()
                    ->from('LR_LIST_TEST_FIELD')
                    ->select('ID')
                    //->where(['NAME' => $value[0], 'LRTID' => $LRTID])
                    ->where(['ENABLED' => '1','NAME' => $value[0]])
                    ->Andwhere(['in', 'LRTID', $LRTID])
                    ->scalar();
            }
			//var_dump($id);
            array_unshift($params[0][$key],$id);
			
        }

        $result = ArrayHelper::map($params[0], '0', '2');

        foreach($result as $key => $value){
            if($value == '0.000' or $value == '0'){
                $result[$key] = 'Не выявлено';//iconv("CP1251", "UTF-8", 'Не выявлено')
            }
            else if(floatval($value) > floatval(0.000)){
                $result[$key] = '10^'.$value;
            }
            else if($value == '-'){
                $result[$key] = 'Отрицательно';//iconv("CP1251", "UTF-8", 'Отрицательно')
            }
            elseif($value == '+'){
                $result[$key] = 'Положительно';//iconv("CP1251", "UTF-8", 'Положительно')
            }

        }
        //var_dump($result);

        return json_encode($result);
    }

}
