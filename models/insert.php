<?php
namespace app\models;

use yii;
use yii\base\Model;
use yii\db\ActiveRecord;

class Insert extends ActiveRecord
{
    public static function tableName()
    {
        return 'LR_RESULT';
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function getMale($MCID)
    {
        $result = (new \yii\db\Query())
            ->select(['GENDER'])
            ->from('MEDCARDS')
            ->where(['ID' => $MCID ,'ENABLED'  => '1'])
            ->scalar();
        if($result === false){
            return false;
        }
        else{
            return $result;
        }

    }

    public function checkRow($LRID, $LRTFID)
    {
        $result = (new \yii\db\Query())
            ->select(['COUNT(*)'])
            ->from('LR_RESULT')
            ->where(['LRID' => $LRID ,'LRTESTFIELDID'  => $LRTFID])
            ->scalar();

        return ($result == 0) ? false : true;
    }

    public function getRowId($LRID, $LRTFID)
    {
        $result = (new \yii\db\Query())
            ->select(['ID'])
            ->from('LR_RESULT')
            ->where(['LRID' => $LRID ,'LRTESTFIELDID'  => $LRTFID])
            ->one();

        return $result['ID'];
    }

    public function getAnalyzeName($LRTFID)
    {
        $result = (new \yii\db\Query())
            ->select(['NAME'])
            ->from('LR_LIST_TEST_FIELD')
            ->where(['ID' => $LRTFID, 'ENABLED' => '1'])
            ->one();

        return $result['NAME'];
    }

    public function getAnalyzeFType($LRTFID)
    {
        $result = (new \yii\db\Query())
            ->select(['FTYPE'])
            ->from('LR_LIST_TEST_FIELD')
            ->where(['ID' => $LRTFID, 'ENABLED' => '1'])
            ->scalar();
        //var_dump($result).'<br>';
        return $result;
    }

    public function getAnalyzeNorm($LRTFID, $VALUE)
    {

        $fType = $this->getAnalyzeFType($LRTFID);

        if($fType !== false){
            if($fType === '0' or $fType === '1' or $fType === '2'){
                //echo 'I`m here<br>';
                $result = (new \yii\db\Query())
                    ->select(['BEGINVALUE', 'ENDVALUE'])
                    ->from('LR_LIST_TEST_FIELD_NORM')
                    ->where(['LRTFID' => $LRTFID, 'ENABLED' => '1'])
                    ->one();
                //var_dump($result).'<br>';
                if($result !== false){
                    //echo floatval($result['BEGINVALUE']). ' '.floatval($VALUE). ' '. floatval($result['ENDVALUE']);
                    if(floatval($result['BEGINVALUE']) <= floatval($VALUE) and floatval($VALUE) <= floatval($result['ENDVALUE'])){
                        return true;
                    }
                    else{
                        return false;
                    }
                }
                else{
                    return false;
                }

            }
            elseif ($fType === '3'){
                $result = (new \yii\db\Query())
                    ->select(['is_norm'])
                    ->from('LR_LIST_TEST_FIELD_VALUES')
                    ->where(['LRTFID' => $LRTFID, 'ENABLED' => '1', 'TEXTVALUE' => $VALUE])
                    ->scalar();

                //echo $LRTFID. ' '. $VALUE. ' ' . $result.'<br>';

                if($result === false){
                    return false;
                }
                else{
                    return ($result === '0') ? false : true;
                }
            }
        }
        else{

            return false;
        }

        return false;
    }



    public function search($params)
    {
        //print_r ($params);

        $LRTESTID = null;
        $NORMTEXT = null;
        $NORMTEXT2 = null;
        $isHasRow = false;
        $ISNORM = false;
        $HTMLDATA = "<HTML><table width='100%' border='3' cellspacing='0' cellpadding='4'>";
        //var_dump($this->getAnalyzeNorm(18, 1111));

        foreach ($params as $key => $value){
            if(is_int($key)){
                //проверяем есть ли запись в табл. LR_RESULT с таким LRID и LRTESTFIELDID
                $isHasRow = $this->checkRow($params['LRID'], $key);

                if($isHasRow){
                    //да есть, значит update
                    $insert = Insert::findOne($this->getRowId($params['LRID'], $key));

                    $HTMLDATA .= "<tr><td>".$this->getAnalyzeName($key);

                    $insert->LRTESTFIELDID = $key;

                    $NORMTEXT = (new \yii\db\Query())
                        ->select(['ID', 'STRVALUE'])
                        ->from('LR_LIST_TEST_FIELD_NORM')
                        ->where(['LRTFID' => $key , 'ENABLED' => '1'])
                        ->one();

                    if($NORMTEXT){
                        $insert->NORMTEXT = $NORMTEXT['STRVALUE'];
                        $insert->LRTESTFIELDNORMID = $NORMTEXT['ID'];
                        $HTMLDATA .= "  (".$NORMTEXT['STRVALUE']."): </td>";

                    }

                    $insert->STRVALUE = $value;
                    $HTMLDATA .= "<td>".$value."</td></tr>";
                    $insert->MCID = $params['MCID'];
                    $insert->ENABLED = 1;

                    $ISNORM = $this->getAnalyzeNorm($key, $value);
                    ($ISNORM == true) ? $insert->ISNOTNORM = 0 : $insert->ISNOTNORM = 1;
                    //var_dump($ISNORM);
                    //echo $key.' '.$insert->ISNOTNORM.'<br>';


                    //$insert->LINK =

                    $insert->update();
                }
                else{

                    //нет, значит insert
                    $insert = new Insert();
                    $insert->LRID = $params['LRID'];

                    $LRTESTID = (new \yii\db\Query())
                        ->select(['LRTID'])
                        ->from('LR_LIST_TEST_FIELD')
                        ->where(['ID' => $key , 'ENABLED' => '1'])
                        ->one();

                    $insert->LRTESTID = $LRTESTID['LRTID'];

                    $insert->LRTESTFIELDID = $key;

                    $NORMTEXT = (new \yii\db\Query())
                        ->select(['ID', 'STRVALUE'])
                        ->from('LR_LIST_TEST_FIELD_NORM')
                        ->where(['LRTFID' => $key , 'ENABLED' => '1'])
                        ->one();


                    if($NORMTEXT){
                        $insert->NORMTEXT = $NORMTEXT['STRVALUE'];
                        $insert->LRTESTFIELDNORMID = $NORMTEXT['ID'];
                    }

                    $insert->STRVALUE = $value;
                    $insert->MCID = $params['MCID'];
                    $insert->ENABLED = 1;
                    $ISNORM = $this->getAnalyzeNorm($key, $value);
                    ($ISNORM == true) ? $insert->ISNOTNORM = 0 : $insert->ISNOTNORM = 1;
                    //var_dump($ISNORM);
                   // echo $key.' '.$insert->ISNOTNORM.'<br>';
                    //$insert->LINK =
                    $insert->save();
                }



            }
        }




        $HTMLDATA .= "</table></HTML>";
        //print_r(Yii::$app->user->id);





        $LR = LABORATORY_RESEARCHES::findOne($params['LRID']);
        $LR->HTMLDATA = $HTMLDATA;
        $LR->UPDUSERID = Yii::$app->user->id;
        $LR->ISDONEUSERID = Yii::$app->user->id;
        ($params['ISDONE'] === 'true') ? $LR->ISDONE = 1 : $LR->ISDONE = 0;
        if($params['ISDONE'] === 'true'){$LR->ENABLED = 1;}


        $LR->update();


        $LRTST = LABORATORY_RESEARCHES_TEST::findOne(['LRID' => $params['LRID']]);
        ($params['ISDONE'] === 'true') ? $LRTST->isdone = 1 : $LRTST->isdone = 0;
		if($params['ISDONE'] === 'true'){$LRTST->ENABLED = 1;}
        
        $LRTST->update();
        
        echo "Запись выполнена";

    }




    /*
    <HTML>
<table width="100%" border="3" cellspacing="0" cellpadding="4">
<tr><td>16, 31, 35  (Отрицательно): </td><td> Отрицательно </td></tr>
<tr><td>33,52,58 (Отрицательно): </td><td> Отрицательно </td></tr>
<tr><td>18,39,45,59 (Отрицательно): </td><td> Отрицательно </td></tr>
</table>
</HTML>
    */
// если нет записей в LR_RESULT то в форме записи/сохранения данных dropdown list без значений, после записи данные появляются. разобратся тащем
}

class LABORATORY_RESEARCHES extends ActiveRecord
{
    public static function tableName()
    {
        return 'LABORATORY_RESEARCHES';
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }


}

class LABORATORY_RESEARCHES_TEST extends ActiveRecord
{
    public static function tableName()
    {
        return 'LABORATORY_RESEARCHES_TEST';
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }


}

class LR_LIST_TEST_FIELD_NORM extends ActiveRecord
{
    public static function tableName()
    {
        return 'LR_LIST_TEST_FIELD_NORM';
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }


}

class LR_LIST_TEST_FIELD_VALUES extends ActiveRecord
{
    public static function tableName()
    {
        return 'LR_LIST_TEST_FIELD_VALUES';
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }


}