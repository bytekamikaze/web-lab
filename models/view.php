<?php
namespace app\models;

use yii;
use yii\base\Model;
use yii\db\ActiveRecord;
use yii\data\ActiveDataProvider;

class View extends ActiveRecord
{
    public static function tableName()
    {
        return 'LR_LIST_TEST_FIELD';
    }

    public function attributeLabels()
    {
        return [
            'NAME' => 'Наименование',
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {

        $this->load($params);

        $lrtid = Lr_Research_Test::find()
            ->select(['LRTESTID'])
            ->where(['LRID' => $params['id']])//LABORATORY_RESEARCH ID
            ->column();

        $query = View::find()
            ->where(['ENABLED' => 1]);

        $query->andFilterWhere([
            'in', 'LRTID', $lrtid
        ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->pagination = false;
        $dataProvider->sort = false;

        return $dataProvider;
    }

    public static function getDropLrListValues($data)
    {

        $query = View::find()
            ->from('LR_LIST_TEST_FIELD_VALUES')
            ->select('TEXTVALUE')
            ->where(['LRTFID' => $data])
            ->column();

        return $query;
    }

    public function getLrResultValue()
    {
        return $this->hasOne(Lr_Result::className(), ['ID' => 'LRTESTFIELDID']);
    }

    public static function getValueLrResult($lrid, $lrtestid, $lrtestfieldid)
    {

        //var_dump($lrid, $lrtestid, $lrtestfieldid);

        $query = Lr_Result::find()
            ->select('STRVALUE')
            ->where([
                'LRID' => $lrid,
                'LRTESTID' => $lrtestid,
                'LRTESTFIELDID' => $lrtestfieldid
            ])
            ->scalar();

        return $query;
    }

    public static function getTestNorm($lrid, $lrtestid, $lrtestfieldid)
    {
        $query = Lr_Result::find()
            ->select('NORMTEXT')
            ->where([
                'LRID' => $lrid,
                'LRTESTID' => $lrtestid,
                'LRTESTFIELDID' => $lrtestfieldid
            ])
            ->scalar();
        
        if ($query == false)
        {
            $query = Lr_List_Field_Test_Norm::find()
                ->select('STRVALUE')
                ->where([
                    'LRTFID' => $lrtestfieldid,
                ])
                ->scalar();
        }

        return $query;
    }

    public static function checkIsDone($id)
    {

        $result = Lr_Research::find()
            ->select('ISDONE')
            ->where(['ID' => $id])
            ->column();

        return $result[0];
    }
}

class Lr_Result extends ActiveRecord
{

    public static function tableName()
    {
        return 'LR_RESULT';
    }

}

class Lr_Research_Test extends ActiveRecord
{

    public static function tableName()
    {
        return 'LABORATORY_RESEARCHES_TEST';
    }

}

class Lr_Research extends ActiveRecord
{

    public static function tableName()
    {
        return 'LABORATORY_RESEARCHES';
    }

}

class Lr_List_Field_Test_Norm extends ActiveRecord
{

    public static function tableName()
    {
        return 'LR_LIST_TEST_FIELD_NORM';
    }

}