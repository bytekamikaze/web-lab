/*-------------------------------------Действия вывода данных---------------------------------------*/

var trTarget = '.tableDirection tbody tr';
var tbodyTarget = '.tableDirection tbody';

//var config = [];

$(trTarget).click(openMainTable);
$(trTarget).mousemove(openPopUp);
$(tbodyTarget).mouseout(closePopUp);

var dataKey;
var urlData;
var fullname;
var fullyears;
var mcid;
var lrlid;
var gendertype;
var barCode;
var fileName;

function openMainTable() {
    dataKey = $(this).attr("data-key");
    urlData = '/site/view/' + dataKey;
    fullname = $(this).children('.fullname').text();
    fullyears = $(this).children('.fullyears').text();
    mcid = $(this).children('.mcid').text();
    lrlid = $(this).children('.lrlistname').attr('lrlid');
    gendertype = $(this).children('.gendertype').text();
    barCode = $(this).children('.barcode').text();
    gendertype = gendertype == "М" ? "Мужской" : gendertype == "Ж" ? "Женский" : "";

    var target = '.mainTable';
    var lrlistname = $(this).children('.lrlistname').text();

    $('#loadingDiv').show();

    $.ajax({
            url: urlData,
            success: function (data) {
                $(target).html(data);
                $(target).dialog({
                    dialogClass: "dialogMain",
                    width: 'auto',
                    height: 'auto',
                    modal: true,
                    buttons: [
                        {
                            text: "Ок",
                            class: 'btn btn-primary btnok',
                            name: dataKey,
                            click: function () {
                                //дествие кнопки OK
                                clickOk(dataKey, mcid, lrlid)
                            }
                        },
                        {
                            text: 'Отмена',
                            class: 'btn btn-default btncancel',
                            click: function () {
                                $(this).dialog("close");
                            }
                        }
                    ]
                });

                $('.loadbtn').click(function () {
                    getFileName(barCode, dataKey);
                });

                $.ajax({
                    //url: 'http://localhost:88/_results/' + barCode + '.xml',
                    url: getAddress() + '/_results/2000000978758.html',
                    type: 'GET',
                    //dataType: 'xml',
                    success: function (data) {
                        $('.linkbtn').show();
                    }
                });
            },
            complete: function () {
                $('.ui-dialog-title').html("Ф.И.О: " + fullname + ", Возраст: " + fullyears + ", Пол: " + gendertype + "</br>" + "<b>" + lrlistname + "</b>");
                $('#loadingDiv').hide();
            }
        }
    );

}

function openPopUp() {

    var target = '.resultTable';
    var htmltext = $(this).children('.htmldata').text();
    var fullname = $(this).children('.fullname').text();

    $(target).html(htmltext);

    $(target).dialog({
        dialogClass: "dialogResult",
        title: fullname,
        width: 'auto',
        height: 'auto',

        position: {
            my: "left+20 bottom-24",
            of: event,
            collision: "fit flip"
        }

    })
}
function closePopUp() {
    var target = '.resultTable';
    $(target).dialog("close");
}
function closeResultModalWindow(){
    var target = '.mainTable';
    $(target).dialog("close");
}

function clickOk() {
    var urlData = '/site/insert/';
    var data = {};
    data['LRID'] = dataKey;
    data['MCID'] = mcid;
    data['LRLID'] = lrlid;
    data['ISDONE'] = $('.done').is(":checked");

    $("tbody").find("input:text, select").each(function () {

        if ($(this).is("input:text")) {
            data[$(this).attr("name")] = $(this).val();
        }
        else if ($(this).is("select")) {
            data[$(this).attr("name")] = $(this).find('option:selected').text();
        }

    });

    $.ajax({
        url: urlData,
        data: data,
        success: function (data) {
            closeResultModalWindow();
            alert(data);
        }
    });
}
function getFileName() {

    var fileType = 'xml';
    var target = '.selectFile';

    var urlData = '/site/file/';
    var data = {};

    data['barcode'] = barCode;
    data['xml'] = fileType;

    $.ajax({
            url: urlData,
            type: 'GET',
            data: data,
            async: false,

            success: function (data) {

                xmlNameArray = JSON.parse(data);
                console.log('Массив файлов где найдены анализы с баркодом -> ' + barCode);
                console.log(xmlNameArray);

                ////console.log(data);

                fileArrLength = xmlNameArray.length;

                if (fileArrLength == 1) {
                    fileName = xmlNameArray[0];
                    insertResult();

                } else if (fileArrLength > 1) {
                    console.log('Количество найденных файлов = ' + fileArrLength);

                    xmlNameArray.forEach(function (item, i, arr) {
                        arr[i] = '<a>' + item + '</a><br>';
                    });

                    //console.log(xmlNameArray);

                    $(target).html(xmlNameArray);
                    $(target).dialog({

                        dialogClass: "dialogSelect",
                        width: 'auto',
                        height: 'auto',
                        modal: true

                    });
                    $('.dialogSelect .ui-dialog-title').html("Выберите файл:");

                    $('.dialogSelect a').on('click', function () {
                        $(target).dialog( "close");
                        fileName = $(this).text();
                        insertResult();
                    });

                } else if (fileArrLength == 0) {
                    alert('Не найдено xml файлов отчета');
                }
            }
        }
    );
}
function insertResult() {

    var urlData = '/site/alias/';
    var data = {};
    data[0] = getResultArray(fileName);
    data['LRID'] = dataKey;
    console.log("Массив данных перед отправкой в обработку alias -> ");
    console.log(data);

    $.ajax({
        url: urlData,
        data: data,

        success: function (data) {

            console.log("Результат alias JSON => ");
            console.log(data);
            resultIdArray = JSON.parse(data);
            console.log('Массив данных с ID анализов');
            console.log(resultIdArray);

            $("tbody").find("input:text, select").each(function () {
                //console.log('input|select id');
                //console.log($(this).attr("name"));

                if (($(this).attr("name") in resultIdArray)) {
                    if ($(this).is("input:text")) {
                        $(this).val(resultIdArray[$(this).attr("name")]);
                    }
                    else if ($(this).is("select")) {
                        $currentIdValue = resultIdArray[$(this).attr("name")];

                        //$(this).find('option').each(function() {
                        // if($(this).text() == $currentIdValue){
                        // $(this).prop("selected", true);
                        // }

                        // });
                        $(this).find('option').filter(function () {
                            return $(this).html() == $currentIdValue;
                        }).prop("selected", true); //work

                    }
                }
            });
        }
    });
}
function getResultArray() {

    var resultArray = [];
    var fileType = 'xml';

    var data = {};

    data['barcode'] = barCode;
    data['xml'] = fileType;

    $.ajax({
        //url: 'http://' + window.location.hostname + '/_results/' + barCode + '.xml',
        url: getAddress() + '/_results/dtlite/' + fileName,
        type: 'GET',
        dataType: fileType,
        async: false,

        success: function (data) {

            var tubes = $('Tubes', data).length;
            var plate = $('plate', data).length;
            
            if(tubes > 0){
                console.log("Тип обрабатываемого файла -> XML");
            }
            else if(plate > 0){
                console.log("Тип обрабатываемого файла -> DNK");
            }
            else{
                console.log("Тип обрабатываемого файла -> Unknown");
            }

            
            //console.log(plate);

            if (tubes > 0) {

                $('Tubes', data).each(function () {

                    if ($('result', this).text().length > 1) {

                        alert("Вы не верное выбрали вариант сохранения xml");
                        return false;

                    } else {
                        var allName = $('id', this).text().split('_');

                        var splitBarCode = allName.pop();
                        var splitName = allName.shift();

                        if (splitBarCode == barCode) {
                            var arr = [];
                            arr.push(splitName);
                            arr.push($('result', this).text());
                            resultArray.push(arr);
                        }
                    }
                });

            } else if (plate > 0) {

                if ($('result', data).text().length == 0) {

                    $('cell', data).each(function () {
                        ////console.log($(this).attr('name'));
                        if ($(this).attr('name') == barCode) {
                            $('test', this).each(function () {

                                var arr = [];

                                arr.push($(this).attr('id').split('\\').pop().replace(/_/g, " "));
                                arr.push($(this).attr('value'));
                                resultArray.push(arr);

                            });
                        }
                        //было
                        /*if ($(this).attr('name') == barCode) {
                         var arr = [];

                         arr.push($('test', this).attr('id').split('\\').pop().replace(/_/g, " "));
                         arr.push($('test', this).attr('value'));
                         resultArray.push(arr);
                         }*/

                    });

                } else {

                    alert("Вы не верно выбрали вариант сохранения xml");
                    return false;
                }
            }
        }
    });
    //console.log('resultArray ->');
    //console.log(resultArray);
    return resultArray;
}
function getAddress() {
    var url = window.location.href;
    var arr = url.split("/");
    var result = arr[0] + "//" + arr[2];
    //console.log(result);
    return result;
}




/*------------------------------------------Загрузка config файла------------------------------------------*/
/*function loadConfig() {
 //console.log('config');
 $.ajax({
 url: getAddress() + '/config/lab.conf',
 type: 'GET',
 dataType: 'xml',
 async: false,

 success: function (data) {
 //console.log('config');
 $('Config', data).each(function () {

 //console.log($(this).children()[0].tagName);
 });


 }
 });
 }*/