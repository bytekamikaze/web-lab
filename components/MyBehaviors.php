<?php

namespace app\components;

use yii\db\ActiveRecord;
use yii\base\Behavior;

class MyBehaviors extends Behavior
{
    private $text;

    public function getTextDate()
    {
        return $this->text;
    }

    public function setTextDate($value)
    {
        $this->text = date('d.m.Y', ($value - 25567)  * 86400);
            //trtotime($value)/86400 + 25567;
    }

    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_FIND => 'afterFind',
        ];
    }

    public function afterFind($event)
    {
        $this->textDate;
    }
}